#!/bin/sh
# GERMAN_CONFIG_FILE="/Applications/Adobe Lightroom/Adobe Lightroom.app/Contents/Resources/de.lproj/TranslatedStrings.txt"
GERMAN_CONFIG_FILE="/Applications/Adobe Lightroom/Adobe Lightroom.app/Contents/Resources/de.lproj/TranslatedStrings_Lr_de_DE.txt"

sed -E -i .bak \
-e 's|^.*ShootArrangement_3/Label.*$|"$$$/AgImportDialog/ShootArrangement_3/Label=Nach Datum: 2005/200512/20051217"|' \
    -e 's|^.*ShootArrangement_3/Template.*$|"$$$/AgImportDialog/ShootArrangement_3/Template=%Y/%Y%m/%Y%m%d"|' \
-e 's|^.*ShootArrangement_4/Label.*$|"$$$/AgImportDialog/ShootArrangement_4/Label=Nach Datum: 2005/200512/20051217_XE2"|' \
    -e 's|^.*ShootArrangement_4/Template.*$|"$$$/AgImportDialog/ShootArrangement_4/Template=%Y/%Y%m/%Y%m%d_XE2"|' \
-e 's|^.*ShootArrangement_9/Label.*$|"$$$/AgImportDialog/ShootArrangement_9/Label=Nach Datum: 2005/200512/20051217_S120"|' \
    -e 's|^.*ShootArrangement_9/Template.*$|"$$$/AgImportDialog/ShootArrangement_9/Template=%Y/%Y%m/%Y%m%d_S120"|' \
-e 's|^.*ShootArrangement_6/Label.*$|"$$$/AgImportDialog/ShootArrangement_6/Label=Nach Datum: 2005/200512/20051217_iPhone"|' \
    -e 's|^.*ShootArrangement_6/Template.*$|"$$$/AgImportDialog/ShootArrangement_6/Template=%Y/%Y%m/%Y%m%d_iPhone"|' \
-e 's|^.*ShootArrangement_7/Label.*$|"$$$/AgImportDialog/ShootArrangement_7/Label=Nach Datum: 2005/200512/20051217_Misc"|' \
    -e 's|^.*ShootArrangement_7/Template.*$|"$$$/AgImportDialog/ShootArrangement_7/Template=%Y/%Y%m/%Y%m%d_Misc"|' \
"$GERMAN_CONFIG_FILE"